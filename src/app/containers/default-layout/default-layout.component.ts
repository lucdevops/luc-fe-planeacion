import { Component, OnInit } from '@angular/core';
import { navItems, navMaestros } from '../../_nav';
import { navBodega } from '../../_nav';
import { navAbastecimiento } from '../../_nav';
import { navVidas } from '../../_nav';
import { Router, NavigationStart } from '@angular/router';

@Component({
  selector: 'app-dashboard',
  templateUrl: './default-layout.component.html'
})
export class DefaultLayoutComponent implements OnInit {

  ruta;

  constructor(private router: Router) {

    router.events.forEach((event) => {
      if (event instanceof NavigationStart) {
        this.ruta = event.url;
      }
    });
  }

  public sidebarMinimized = false;
  public navItems = navItems;
  public navBodega = navBodega;
  public navAbastecimiento = navAbastecimiento;
  public navVidas = navVidas;
  public navMaestros = navMaestros;


  toggleMinimize(e) {
    this.sidebarMinimized = e;
  }

  ngOnInit() {
    this.ruta = this.router.url;
  }

}
