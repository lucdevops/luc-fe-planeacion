import { Rutas } from './../../../rutas';
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Prods } from '../../../model/prods';
import { Observable, merge } from 'rxjs';
import { Referencias } from '../../../model/referencias';

@Injectable()
export class InsertProdsService {

  valorNeto;
  referencias;
  lista;
  vendedor;

  constructor(private http: HttpClient, private r: Rutas) { }

  cargarRefe(): Observable<Referencias[]> {
    return this.http.get<Referencias[]>(this.r['datosWMS']  + '/cargaAllRefe');
  }

  crearPedidoWMS(proveedor, agendados) {
    return this.http.get(this.r['crearWMS']  + '/crearPedidoWMS/' + agendados + '/' + proveedor);
  }

  cambiarEstadoRec(pedido) {
    return this.http.get(this.r['datosWMS']  + '/modificarEstadoRec/' + pedido);
  }

  crearRecepcion(prods: Prods, nit: string) {
    let separador = '';
    let producto = '';

    // tslint:disable-next-line: prefer-for-of
    for (let i = 0; i < prods.items.length; i++) {

      if (producto === '') {
        separador = '';
      } else {
        separador = ',';
      }

      // tslint:disable-next-line: max-line-length
      producto = producto + separador + '{\"iinventario\":\"1101\",\"irecurso\":\"' + prods.items[i].producto + '\",\"itiporec\":\"\",\"qrecurso\":\"' + prods.items[i].cantidad + '\",\"icc\":\"\",\"sobserv\":\"\"}';
    }

    return this.http.get(this.r['crearCP']  + '/crearRecepcion/' + producto + '/2/1101/' + nit);
  }

  /*crearRecepcion(prods: Prods, nit: string, productos: string) {

      let separador = '';
      let producto = '';

      // tslint:disable-next-line: prefer-for-of
      for (let i = 0; i < prods.items.length; i++) {

          if (producto === '') {
              separador = '';
          } else {
              separador = ',';
          }

          // tslint:disable-next-line: max-line-length
          producto = producto + separador + '{\"iinventario\":\"1101\",\"irecurso\":\"' + prods.items[i].producto + '\",\"itiporec\":\"\",\"qrecurso\":\"' + prods.items[i].cantidad + '\",\"icc\":\"\",\"sobserv\":\"\"}';
      }

      this.http.get(this.crearCP + '/crearRecepcion/' + producto + '/2/1101/805013107/' + nit).pipe(
          map(resultCP => {
              console.log(resultCP);
              return resultCP;
          }), mergeMap(resultCP => this.http.get(this.datosWMS + '/infoTercero/' + nit).pipe(
              map(resultIT => {
                  console.log(resultIT);
                  this.lista = resultIT[0];
                  this.vendedor = resultIT[1];
                  return resultIT;
              })
          )), mergeMap(resultIT => this.http.get(this.datosCP + '/referencia/' + resultIT[0] + '/' + nit + '/1101/' + productos).pipe(
              map(resultRef => {
                  console.log(resultRef);
                  this.valorNeto = resultRef[1];
                  this.referencias = resultRef[0];
                  return resultRef;
              })

              // tslint:disable-next-line: max-line-length
          )), mergeMap(resultIT => this.http.get(this.crearCP + '/crearFactura/' + this.referencias + '/2/17/1101/' + nit + '/' + resultIT[1] + '/' + this.valorNeto + '/' + this.lista).pipe(
              map(resultF => {
                  console.log(resultF);
              })
          ))
      ).subscribe();


      // tslint:disable-next-line: max-line-length
      //return this.http.get(this.crearCP + '/crearFactura/' + this.referencias + '/2/17/1101/' + nit + '/' + this.vendedor + '/' + this.valorNeto + '/' + this.lista);


  }*/

  /*infoTercero(nit: string) {
      return this.http.get(this.datosWMS + '/infoTercero/' + nit);
  }

  precioRef(lista: string, productos: string, nit: string) {
      return this.http.get(this.datosCP + '/referencia/' + lista + '/' + nit + '/1101/' + productos);
  }

  crearFactura(vendedor: string, nit: string, productos: string, valorNeto: string, lista: string) {
      // tslint:disable-next-line: max-line-length
      return this.http.get(this.crearCP + '/crearFactura/' + productos + '/2/17/1101/' + nit + '/' + vendedor + '/' + valorNeto + '/' + lista);
  }*/

}
