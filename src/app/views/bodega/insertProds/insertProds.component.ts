import { Rutas } from './../../../rutas';
import { Component, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup, FormArray, Validators } from '@angular/forms';
import { Prods } from '../../../model/prods';
import { InsertProdsService } from './insertProds.service';
import { ActivatedRoute } from '@angular/router';
import { Referencias } from '../../../model/referencias';
import { HttpClient } from '@angular/common/http';
import { mergeMap, map } from 'rxjs/operators';
import { ModalDirective } from 'ngx-bootstrap/modal';

@Component({
  selector: 'app-product',
  templateUrl: './insertProds.component.html',
})
export class InsertProdsComponent implements OnInit {

  prods: Prods[];
  refe: Referencias[];
  prod: number;
  pointIndex = 0;
  cont;
  cnt: number;
  result: any = '';
  miFormulario: FormGroup;
  closeResult: string;
  nit: any;
  numeroPed: any;
  numeroFac: any;
  numRecep: string;
  focus: any;
  lista: string;
  control: boolean;
  referencias;
  vendedor;
  valorNeto;

  public isShowingSecond: boolean;
  public loading: boolean;

  constructor(
    private fb: FormBuilder,
    private prodService: InsertProdsService,
    private activatedRoute: ActivatedRoute,
    private http: HttpClient,
    private r: Rutas) {

    this.focus = '';
    this.isShowingSecond = false;
    this.nit = this.activatedRoute.snapshot.paramMap.get('nit');
    this.numeroPed = this.activatedRoute.snapshot.paramMap.get('numeroPed');
    this.numeroFac = this.activatedRoute.snapshot.paramMap.get('numeroFac');
  }

  ngOnInit() {

    this.miFormulario = this.fb.group({
      items: this.fb.array([this.fb.group({
        cantidad: ['', [Validators.required]],
        producto: ['', [Validators.required, Validators.maxLength(13), Validators.minLength(13)]]
      })])
    });

    this.prodService.cargarRefe().subscribe(r => {
      this.refe = r;
    });
  }

  submit(formValue: any) {

    let separador = '';
    let productos = '';
    const prods = new Prods();
    prods.items = formValue.items;
    this.loading = true;

    if (this.nit === '805013107') {
      this.prodService.crearRecepcion(prods, this.nit).subscribe(r => {
        window.location.href = '#/bodega/datos/' + r[1] + '/1234';
      });
    } else {
      if (this.miFormulario.valid) {
        // tslint:disable-next-line: prefer-for-of
        for (let i = 0; i < prods.items.length; i++) {
          if (productos === '') {
            separador = '';
          } else {
            separador = '';
          }
          productos = productos + separador + '{' + prods.items[i].producto + '-' + prods.items[i].cantidad + '}';
        }

        if (this.numeroFac === this.numeroPed) {
          this.prodService.crearPedidoWMS('VITAAVE', this.nit).subscribe();
        }

        this.http.get(this.r['datosWMS'] + '/infoTercero/' + this.nit).pipe(
          map(resultIT => {
            this.lista = resultIT[0];
            this.vendedor = resultIT[1];
            return resultIT;
            // tslint:disable-next-line: max-line-length
          }), mergeMap(resultIT => this.http.get(this.r['datosCP'] + '/referencia/' + resultIT[0] + '/' + this.nit + '/1101/' + productos).pipe(
            map(resultRef => {
              this.valorNeto = resultRef[1];
              this.referencias = resultRef[0];
              return resultRef;
            })
            // tslint:disable-next-line: max-line-length
          )), mergeMap(resultIT => this.http.get(this.r['crearCP'] + '/crearFactura/' + this.referencias + '/2/17/1101/' + this.nit + '/' + this.valorNeto + '/' + this.lista + '/' + this.numeroFac + '/' + this.numeroPed).pipe(
            map(resultF => {
              return resultF;
            })
            // tslint:disable-next-line: max-line-length
          )), mergeMap(resultF => this.http.get(this.r['crearWMS'] + '/crearFacturaWms/' + this.referencias + '/' + this.valorNeto + '/' + this.vendedor + '/' + this.numeroPed + '/' + this.nit).pipe(
            map(resultMod => {
              return resultMod;
            })
          ))
        ).subscribe(resultMod => {
          window.location.href = '#/bodega/datos/12345/' + this.numeroFac;
        });

      }
    }
  }

  get getItems() {
    return this.miFormulario.get('items') as FormArray;
  }

  validar(codigo) {
    this.cont = 0;
    // tslint:disable-next-line: prefer-for-of
    for (let index = 0; index < this.refe.length; index++) {
      // tslint:disable-next-line: no-conditional-assignment
      if (this.refe[index].codigo === codigo) {
        this.cont++;
      }
    }
  }

  addItem() {
    const control = <FormArray>this.miFormulario.controls['items'];
    control.push(this.fb.group({ cantidad: [], producto: [] }));
    this.pointIndex++;
  }

  removeItem(index: number) {
    const control = <FormArray>this.miFormulario.controls['items'];
    control.removeAt(index);
    this.pointIndex--;
  }

  public setFocus(fieldToFocus: number): void {
    this.focus = fieldToFocus;
  }

  public toggleSecond(fieldToFocus: number): void {
    this.isShowingSecond = !this.isShowingSecond;
    this.setFocus(fieldToFocus);
  }

  onInputEntry(event, nextInput) {

    let input = event.target;
    let length = input.value.length;
    let maxLength = input.attributes.maxlength.value;

    if (length >= maxLength) {
      nextInput.focus();
    }
  }
}
