import { LoteRepComponent } from './recepcion/loteRep/loteRep.component';
import { DetalleRepComponent } from './recepcion/detalleRep/detalleRep.component';
import { RecepcionComponent } from './recepcion/recepcion.component';
import { DatosComponent } from './datos/datos.component';
import { HttpClientModule } from '@angular/common/http';
import { AutofocusDirective } from './../../directives/autofocus.directive';
import { DemoMaterialModule } from './../../material-module';
// Angular
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgModule } from '@angular/core';
import { InsertProdsComponent } from './insertProds/insertProds.component';
import { AgendadosComponent } from './agendados/agendados.component';
import { BsDropdownModule } from 'ngx-bootstrap/dropdown';
import { BodegaRoutingModule } from './bodega-routing.module';
import { RouterModule } from '@angular/router';
import { ModalModule } from 'ngx-bootstrap/modal';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    BodegaRoutingModule,
    BsDropdownModule.forRoot(),
    ModalModule.forRoot(),
    DemoMaterialModule,
    ReactiveFormsModule,
    HttpClientModule,
    RouterModule
  ],
  declarations: [
    InsertProdsComponent,
    AgendadosComponent,
    DatosComponent,
    RecepcionComponent,
    DetalleRepComponent,
    LoteRepComponent,
    AutofocusDirective
  ]
})
export class BodegaModule { }
