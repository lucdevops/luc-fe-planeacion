import { Component, OnInit } from '@angular/core';
import { AgendadosService } from './agendados.service';
import { Cliente } from '../../../model/clientes';
import { FormGroup, FormControl, Validators, FormBuilder } from '@angular/forms';
import { Observable } from 'rxjs';
import { map, flatMap } from 'rxjs/operators';

@Component({
  templateUrl: './agendados.component.html'
})
export class AgendadosComponent implements OnInit {

  miFormulario: FormGroup;
  filteredref: Observable<Cliente[]>;
  myControl = new FormControl();
  mostrar: boolean = false;
  value = '';
  nit;
  numPed;
  numFac;
  tercerosDia: Cliente[];
  day = new Date().getDate();
  month = new Date().getMonth() + 1;
  year = new Date().getFullYear();

  constructor(private agendadosService: AgendadosService, private fb: FormBuilder) {
  }

  ngOnInit() {

    this.miFormulario = this.fb.group({
      numFac: ['', Validators.required]
    });

    this.agendadosService.cargarCliDiaAgen().subscribe(cd => {
      this.tercerosDia = cd;
    });

    this.filteredref = this.myControl.valueChanges
      .pipe(
        map(value => typeof value === 'string' ? value : value.descripcion),
        flatMap(value => value ? this.filterref(value) : [])
      );
  }

  filterref(value: string): Observable<Cliente[]> {
    const filterValue = value.toLowerCase();
    return this.agendadosService.cargaCli(filterValue);
  }

  datos(ter: Cliente) {
    this.nit = ter.nit;
    this.numPed = ter.autoretenedor;
  }

  submit(formValue: any) {
    window.location.href = '#/bodega/consolidado/' + this.nit + '/' + this.numPed + '/' + formValue.numFac;
  }

  cambiarClase() {
    this.mostrar = true;
  }

  enviar() {
    window.location.href = '#/bodega/consolidado/' + this.myControl.value + '/' + this.numFac + '/' + this.numFac;
  }

}
