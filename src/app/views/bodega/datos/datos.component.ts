import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-datos',
  templateUrl: './datos.component.html',
})
export class DatosComponent implements OnInit {

  rec: any;
  fac: any;

  constructor(private activatedRoute: ActivatedRoute) {
    this.rec = this.activatedRoute.snapshot.paramMap.get('operRec');
    this.fac = this.activatedRoute.snapshot.paramMap.get('operFac');
  }

  ngOnInit() {
  }

}
