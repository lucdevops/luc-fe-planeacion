import { LoteRepComponent } from './recepcion/loteRep/loteRep.component';
import { DetalleRepComponent } from './recepcion/detalleRep/detalleRep.component';
import { RecepcionComponent } from './recepcion/recepcion.component';
import { DatosComponent } from './datos/datos.component';
import { AgendadosComponent } from './agendados/agendados.component';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { InsertProdsComponent } from './insertProds/insertProds.component';

const routes: Routes = [
  {
    path: '',
    data: {
      title: 'Bodega'
    },
    children: [
      /*{
        path: '',
        redirectTo: 'agendados'
      },*/
      {
        path: 'agendados',
        component: AgendadosComponent,
        data: {
          title: 'Agendados'
        }
      },
      {
        path: 'recepcion',
        component: RecepcionComponent,
        data: {
          title: 'Recepcion'
        }
      },
      {
        path: 'consolidado/:nit/:numeroPed/:numeroFac',
        component: InsertProdsComponent,
        data: {
          title: 'Consolidado'
        }
      },
      {
        path: 'datos/:operRec/:operFac',
        component: DatosComponent,
        data: {
          title: 'Datos'
        }
      },
      {
        path: 'detalleRep/:numero/:tipo',
        component: DetalleRepComponent,
        data: {
          title: 'Det Orden de Compra'
        }
      },
      {
        path: 'loteRep/:codigo/:cantidad/:tipo/:numero',
        component: LoteRepComponent,
        data: {
          title: 'Ingreso de lotes'
        }
      },
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class BodegaRoutingModule { }
