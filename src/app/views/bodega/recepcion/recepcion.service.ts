import { Rutas } from './../../../rutas';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Ped } from '../../../model/ped';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class RecepcionService {

  constructor(private http: HttpClient, private r: Rutas) { }

  consultarOrdenes(): Observable<Ped[]> {
    return this.http.get<Ped[]>(this.r['datosWMS'] + '/cargarOrdenes');
  }

  guardarFac(numero, tipo, numFac) {
    return this.http.get(this.r['crearWMS'] + '/guardarFac/' + numero + '/' + tipo + '/' + numFac);
  }

}
