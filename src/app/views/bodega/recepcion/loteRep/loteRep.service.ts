import { Rutas } from './../../../../rutas';
import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Um } from '../../../../model/um';

@Injectable({
  providedIn: 'root'
})
export class LoteRepService {

  constructor(private http: HttpClient, private r: Rutas) { }

  consultarUm(codigo: string): Observable<Um[]> {
    return this.http.get<Um[]>(this.r['datosWMS'] + '/consultarUm/' + codigo);
  }

  guardarLoteOrc(tipo: string, numero: number, codigo: string, cantidad: number, lote: string, fecha: number, total: number) {
    // tslint:disable-next-line: max-line-length
    return this.http.get<any>(this.r['crearWMS'] + '/guardarLoteOrc/' + tipo + '/' + numero + '/' + codigo + '/' + cantidad + '/' + lote + '/' + fecha + '/' + total);
  }

}
