import { Component, OnInit } from '@angular/core';
import { RecepcionService } from './recepcion.service';
import { Ped } from '../../../model/ped';

@Component({
  templateUrl: './recepcion.component.html',
})
export class RecepcionComponent implements OnInit {

  orc: Ped[] = [];
  numero;
  tipo;
  numFac: string;

  constructor(private recepService: RecepcionService) { }

  ngOnInit() {
    this.recepService.consultarOrdenes().subscribe(r => {
      this.orc = r;
    });
  }

  asignar(numero, tipo) {
    this.numero = numero;
    this.tipo = tipo;
  }

  guardarFac() {
    this.recepService.guardarFac(this.numero, this.tipo, this.numFac).subscribe(r => {
      window.location.href = '#/bodega/detalleRep/' + this.numero + '/' + this.tipo;
    });

  }

}
