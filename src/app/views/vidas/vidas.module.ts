import { ProgressbarModule } from 'ngx-bootstrap/progressbar';
import { RutasComponent } from './rutas/rutas.component';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { BsDropdownModule } from 'ngx-bootstrap/dropdown';
import { VidasRoutingModule } from './vidas-routing.module';
import { ModalModule } from 'ngx-bootstrap/modal';
import { DemoMaterialModule } from '../../material-module';
import { CommonModule, DatePipe } from '@angular/common';
import { HttpClientModule } from '@angular/common/http';
import { RouterModule } from '@angular/router';
import { PresupuestoComponent } from './presupuesto/presupuesto.component';
import { TabsModule } from 'ngx-bootstrap/tabs';
import { CollapseModule } from 'ngx-bootstrap/collapse';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';

@NgModule({
  imports: [
    VidasRoutingModule,
    CommonModule,
    FormsModule,
    BsDropdownModule.forRoot(),
    ModalModule.forRoot(),
    CollapseModule.forRoot(),
    ProgressbarModule.forRoot(),
    DemoMaterialModule,
    ReactiveFormsModule,
    NgbModule,
    HttpClientModule,
    TabsModule,
    RouterModule
  ],
  declarations: [RutasComponent, PresupuestoComponent],
  providers: [DatePipe]
})
export class VidasModule { }
