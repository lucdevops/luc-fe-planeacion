import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Rutas } from '../../../rutas';
import { Observable } from 'rxjs';
import { Cliente } from '../../../model/clientes';

@Injectable({
  providedIn: 'root'
})
export class RutasService {

  constructor(private http: HttpClient, private r: Rutas) {
  }

  cargaCli(descripcion): Observable<Cliente[]> {
    return this.http.get<Cliente[]>(this.r['datosWMS'] + '/cargaCli/' + descripcion);
  }

  cargarVidas(): Observable<Cliente[]> {
    return this.http.get<Cliente[]>(this.r['datosWMS'] + '/cargarVidas');
  }

  cargarRuta(vendedor: number): Observable<Cliente[]> {
    return this.http.get<Cliente[]>(this.r['datosWMS'] + '/cargarRuta/' + vendedor);
  }

  agregarAlmacen(vendedor: number, almacen: string) {
    return this.http.get(this.r['crearWMS'] + '/agregarAlmacen/' + vendedor + '/' + almacen);
  }

  guardarFechaIFechaF(id: number, fechaI: string, fechaF: string) {
    return this.http.get(this.r['crearWMS'] + '/guardarFechaIFechaF/' + id + '/' + fechaI + '/' + fechaF);
  }

}
