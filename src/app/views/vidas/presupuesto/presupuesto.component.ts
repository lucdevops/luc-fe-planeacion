import { AppModule } from './../../../app.module';
import { Rutas } from './../../../rutas';
import { Component, OnInit } from "@angular/core";
import { PresupuestoService } from "./presupuesto.service";
import { Cliente } from "../../../model/clientes";
import { HttpClient } from '@angular/common/http';
import { map, mergeMap } from 'rxjs/operators';
import { MomentDateAdapter, MAT_MOMENT_DATE_ADAPTER_OPTIONS } from '@angular/material-moment-adapter';
import { DateAdapter, MAT_DATE_FORMATS, MAT_DATE_LOCALE } from '@angular/material/core';
import { MatDatepicker } from '@angular/material/datepicker';
import * as _moment from 'moment';
// tslint:disable-next-line:no-duplicate-imports
import { default as _rollupMoment, Moment } from 'moment';
import { FormControl } from '@angular/forms';
import { DatePipe } from '@angular/common';



const moment = _rollupMoment || _moment;

// See the Moment.js docs for the meaning of these formats:
// https://momentjs.com/docs/#/displaying/format/
export const MY_FORMATS = {
  parse: {
    dateInput: 'MM/YYYY',
  },
  display: {
    dateInput: 'MM/YYYY',
    monthYearLabel: 'MMM YYYY',
    dateA11yLabel: 'LL',
    monthYearA11yLabel: 'MMMM YYYY',
  },
};

@Component({
  selector: 'app-presupuesto',
  templateUrl: './presupuesto.component.html',
  providers: [
    // `MomentDateAdapter` can be automatically provided by importing `MomentDateModule` in your
    // application's root module. We provide it at the component level here, due to limitations of
    // our example generation script.
    {
      provide: DateAdapter,
      useClass: MomentDateAdapter,
      deps: [MAT_DATE_LOCALE, MAT_MOMENT_DATE_ADAPTER_OPTIONS]
    },

    { provide: MAT_DATE_FORMATS, useValue: MY_FORMATS },
  ],
})
export class PresupuestoComponent implements OnInit {
  vidas: Cliente[];
  presupuestoP: any[];
  presupuestoC: any[];
  detalleC: any[];
  mercaderista;

  constructor(private presupuestoService: PresupuestoService, private http: HttpClient, private r: Rutas, private dp: DatePipe) { }

  date = new FormControl(moment());

  chosenYearHandler(normalizedYear: Moment) {
    const ctrlValue = this.date.value;
    ctrlValue.year(normalizedYear.year());
    this.date.setValue(ctrlValue);
  }

  chosenMonthHandler(normalizedMonth: Moment, datepicker: MatDatepicker<Moment>) {
    const ctrlValue = this.date.value;
    ctrlValue.month(normalizedMonth.month());
    this.date.setValue(ctrlValue);
    datepicker.close();
  }

  ngOnInit() {
    this.presupuestoService.cargarVidas().subscribe((v) => {
      this.vidas = v;
    });
  }

  cargarRuta() {
    this.presupuestoService
      .cargarPresupuestoProveedor(this.mercaderista, this.date.value)
      .subscribe((r) => {
        this.presupuestoP = r;
        for (let i = 0; i < this.presupuestoP.length; i++) {

          // tslint:disable-next-line: max-line-length
          this.http.get(this.r['datosWMS'] + '/cargarCntFacDevProv/' + this.mercaderista + '/' + this.presupuestoP[i][0] + '/' + this.dp.transform(this.date.value, 'yyyy-MM').toString()).pipe(
            map(l => {
              this.presupuestoP[i]['data'] = l;
              let cntFac = 0, cntDev = 0;
              for (let j = 0; j < this.presupuestoP[i]['data'].length; j++) {

                if (this.presupuestoP[i]['data'][j][1] === 'FAC') {
                  // tslint:disable-next-line: radix
                  cntFac = parseInt(this.presupuestoP[i]['data'][j][2]);
                } else {
                  // tslint:disable-next-line: radix
                  cntDev = parseInt(this.presupuestoP[i]['data'][j][2]);
                }
              }

              this.presupuestoP[i]['fac'] = cntFac;
              this.presupuestoP[i]['dev'] = cntDev;

            })
          ).subscribe();
        }
      });


    this.presupuestoService
      .cargarPresupuestoCliente(this.mercaderista, this.date.value)
      .subscribe((c) => {
        this.presupuestoC = c;
        let marcas = [];
        for (let i = 0; i < this.presupuestoC.length; i++) {
          // tslint:disable-next-line: max-line-length
          this.http.get(this.r['datosWMS'] + '/cargarDetalleCliente/' + this.mercaderista + '/' + this.dp.transform(this.date.value, 'yyyy-MM').toString() + '/' + this.presupuestoC[i][0]).pipe(
            map(r => {
              this.presupuestoC[i]['det'] = r;
              for (let j = 0; j < this.presupuestoC[i]['det'].length; j++) {
                // tslint:disable-next-line: max-line-length
                this.http.get(this.r['datosWMS'] + '/cargarCntFacDevProv/' + this.mercaderista + '/' + this.presupuestoC[i][0] + '/' + this.presupuestoC[i]['det'][j][0] + '/' + this.dp.transform(this.date.value, 'yyyy-MM').toString()).pipe(
                  map(l => {
                    console.log(l);
                  })

                ).subscribe();


              }

            })
          ).subscribe();



          //console.log(this.presupuestoC[i]);


          /*for (let j = 0; j < this.presupuestoC[i]['det'].length; j++) {
            console.log(this.presupuestoC[i]['det'][j]);
            // tslint:disable-next-line: max-line-length
            this.http.get(this.r['datosWMS'] + '/cargarCntFacDevProv/' + this.mercaderista + '/' + this.presupuestoC[i][0] + '/' + this.presupuestoC[i]['det'][j][0] + '/' + this.dp.transform(this.date.value, 'yyyy-MM').toString()).pipe(
              map(l => {
                console.log(l);
              })
            ).subscribe();
          }*/
        }
      });
  }

}
