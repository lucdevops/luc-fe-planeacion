import { Rutas } from './../../../rutas';
import { Ped } from './../../../model/ped';
import { PreOrden } from './../../../model/preOrden';
import { Observable } from 'rxjs';
import { Estimado } from './../../../model/estimado';
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class RequerimientosService {

  constructor(private http: HttpClient, private r: Rutas) { }

  consultar(form: any): Observable<PreOrden[]> {
    return this.http.get<PreOrden[]>(this.r['datosPlaneacion'] + '/preOrden/' + form.mar);
  }

  consultarPreOrc(): Observable<Ped[]> {
    return this.http.get<Ped[]>(this.r['datosPlaneacion'] + '/cargarPreOrden');
  }

  datoMes() {
    return this.http.get(this.r['datosPlaneacion'] + '/datoMes');
  }

}
