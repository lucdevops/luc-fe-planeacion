import { Rutas } from './../../../rutas';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { DetPed } from '../../../model/detPed';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class DetalleService {

  constructor(private http: HttpClient, private r: Rutas) { }

  consultarDet(numero: number, tipo: string): Observable<DetPed[]> {
    return this.http.get<DetPed[]>(this.r['datosPlaneacion'] + '/detPreOrden/' + numero + '/' + tipo);
  }

  modificarCantidad(numero: number, tipo: string, cnt: string, codigo: string) {
    return this.http.get(this.r['crearPlaneacion'] + '/cambiarDetPreOrden/' + numero + '/' + tipo + '/' + cnt + '/' + codigo);
  }

  generarOrc(detalleOrc: string, numero: number, tipo: string): Observable<DetPed[]> {
    return this.http.get<DetPed[]>(this.r['crearCP'] + '/crearOrc/' + detalleOrc + '/' + numero + '/' + tipo);
  }

  rechazarOrc(numero: number, tipo: string) {
    return this.http.get(this.r['datosPlaneacion'] + '/rechazarOrc/' + numero + '/' + tipo);
  }

  calcularQuincena(numero: number, tipo: string) {
    return this.http.get(this.r['datosPlaneacion']+ '/calcularQuincena/'+ numero + '/' + tipo);
  }
}
