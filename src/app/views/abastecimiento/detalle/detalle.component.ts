import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { DetalleService } from './detalle.service';
import { DetPed } from '../../../model/detPed';

@Component({
  templateUrl: './detalle.component.html'
})
export class DetalleComponent implements OnInit {

  numero: number;
  tipo: string;
  det: DetPed[];
  public loading: boolean;
  quincena: any;
  t: any;


  constructor(private rutaActiva: ActivatedRoute, private detPedService: DetalleService) { }

  ngOnInit() {
    this.numero = this.rutaActiva.snapshot.params.numero;
    this.tipo = this.rutaActiva.snapshot.params.tipo;

    this.detPedService.consultarDet(this.numero, this.tipo).subscribe(r => {
      this.det = r;
    });

    this.detPedService.calcularQuincena(this.numero, this.tipo).subscribe(r=> {
      this.quincena = r;
    });


  }

  habilitar(i: string) {
    const validation = document.getElementById(i).getAttribute('disabled');
    const codigo = document.getElementById(i).getAttribute('name');

    //console.log(validation);
    if (validation === 'disabled') {
      document.getElementById(i).removeAttribute('disabled');
      document.getElementById(i).focus();
    } else {
      let inputValue = (document.getElementById(i) as HTMLInputElement).value;
      document.getElementById(i).setAttribute('disabled', 'disabled');
      this.detPedService.modificarCantidad(this.numero, this.tipo, inputValue, codigo).subscribe();
    }

  }

  generarOrc() {
    this.loading = true;
    let separador = '';
    let productos = '';

    this.detPedService.consultarDet(this.numero, this.tipo).subscribe(r => {
      this.det = r;
      // tslint:disable-next-line: prefer-for-of
      for (let i = 0; i < r.length; i++) {
        if (r[i].cantidadDes !== 0) {
          if (productos === '') {
            separador = '';
          } else {
            separador = '';
          }
          productos = productos + separador + '{' + r[i].refe.codigo + '-' + r[i].cantidadDes + '}';
        }

        
      }
      console.log(productos);

      this.detPedService.generarOrc(productos, this.numero, this.tipo).subscribe(t => {
        this.t = t;
        if (this.t === 'true') {

          this.t = true;
        }
        this.loading = false;
      });
    });

  }

  rechazarOrc() {
    this.detPedService.rechazarOrc(this.numero, this.tipo).subscribe(r => {
      window.location.href = '#/abastecimiento/requerimientos';
    });
  }


  fin() {
    window.location.href = '#/abastecimiento/requerimientos';
  }

  redondear(value: number) {
    //console.log(value);
    let r = Math.round(value);
    if (r === 0) {
      return Math.ceil(value);
    } else {
      return Math.round(value);
    }
  }

}
