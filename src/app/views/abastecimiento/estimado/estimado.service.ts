import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable, forkJoin } from 'rxjs';
import { Cliente } from '../../../model/clientes';
import { Marca } from '../../../model/marca';
import { Referencias } from '../../../model/referencias';
import { DatePipe } from '@angular/common';
import { Rutas } from '../../../rutas';

@Injectable({
  providedIn: 'root'
})
export class EstimadoService {

  constructor(private http: HttpClient, private dp: DatePipe, private r: Rutas) { }

  cargarMar(descripcion): Observable<Marca[]> {
    return this.http.get<Marca[]>(this.r['datosWMS'] + '/cargaMar/' + descripcion);
  }

  cargaCli(descripcion): Observable<Cliente[]> {
    return this.http.get<Cliente[]>(this.r['datosWMS'] + '/cargaCli/' + descripcion);
  }

  cargaRef(descripcion): Observable<Referencias[]> {
    return this.http.get<Referencias[]>(this.r['datosWMS'] + '/cargaRefe/' + descripcion);
  }

  cargarEstimado(estimados: any, form: any, clien: any, refe: any) {
    let total = 0;
    let ruta;

    if (clien === null && refe === null) {
      let rutas = [];

      // tslint:disable-next-line: prefer-for-of
      for (let i = 0; i < estimados.length; i++) {

        let dis = estimados[i].dis.split(',');
        let rep = estimados[i].rep.split(',');
        let totalDis = '', totalRep = '';

        for (let j = 0; j < dis.length; j++) {
          totalDis = totalDis + (dis[j]);
        }

        for (let j = 0; j < rep.length; j++) {
          totalRep = totalRep + (rep[j]);
        }

        // tslint:disable-next-line: radix
        total = parseInt(totalDis) + parseInt(totalRep);

        ruta = this.dp.transform(form, 'yyyy/MM').toString() + '/' + estimados[i].prov + '/' + total + '/' + clien + '/' + refe;
        let rute = this.http.get(this.r['datosPlaneacion'] + '/cargaEstimado/' + ruta);
        rutas.push(rute);
      }

      return forkJoin(rutas);
    }


    if (refe === null) {

      let rutas = [];
      // tslint:disable-next-line: prefer-for-of
      for (let i = 0; i < estimados.length; i++) {

        let dis = estimados[i].dis.split(',');
        let rep = estimados[i].rep.split(',');
        let totalDis = '', totalRep = '';

        for (let j = 0; j < dis.length; j++) {
          totalDis = totalDis + (dis[j]);
        }

        for (let j = 0; j < rep.length; j++) {
          totalRep = totalRep + (rep[j]);
        }

        // tslint:disable-next-line: radix
        total = parseInt(totalDis) + parseInt(totalRep);

        ruta = this.dp.transform(form, 'yyyy/MM').toString() + '/' + estimados[i].prov + '/' + total + '/' + clien + '/' + refe;
        let rute = this.http.get(this.r['datosPlaneacion'] + '/cargaEstimado/' + ruta);
        rutas.push(rute);
      }

      return forkJoin(rutas);
    }

    if (estimados === null && clien === null) {

      let dis = form.dis.split(',');
      let rep = form.rep.split(',');
      let totalDis = '', totalRep = '';

      for (let j = 0; j < dis.length; j++) {
        totalDis = totalDis + (dis[j]);
      }

      for (let j = 0; j < rep.length; j++) {
        totalRep = totalRep + (rep[j]);
      }

      // tslint:disable-next-line: radix
      total = parseInt(totalDis) + parseInt(totalRep);

      ruta = this.dp.transform(form.fecha.value, 'yyyy/MM').toString() + '/0/' + total + '/' + clien + '/' + refe;
      return this.http.get(this.r['datosPlaneacion'] + '/cargaEstimado/' + ruta);
    }

    if (estimados === null) {

      let dis = form.dis.split(',');
      let rep = form.rep.split(',');
      let totalDis = '', totalRep = '';

      for (let j = 0; j < dis.length; j++) {
        totalDis = totalDis + (dis[j]);
      }

      for (let j = 0; j < rep.length; j++) {
        totalRep = totalRep + (rep[j]);
      }

      // tslint:disable-next-line: radix
      total = parseInt(totalDis) + parseInt(totalRep);

      /*total = form.dis + form.rep;
      ruta = this.dp.transform(form.fecha.value, 'yyyy/MM').toString() + '/0/' + total + '/' + clien + '/' + refe;
      return this.http.get(this.r['datosPlaneacion'] + '/cargaEstimado/' + ruta);*/
    }

  }

}
