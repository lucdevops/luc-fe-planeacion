import { Component, OnInit } from '@angular/core';
import { EstimadoService } from './estimado.service';
import { Cliente } from '../../../model/clientes';
import { FormGroup, FormControl, Validators, FormBuilder, FormArray } from '@angular/forms';
import { Observable } from 'rxjs';
import { map, flatMap, startWith } from 'rxjs/operators';
import { Marca } from '../../../model/marca';
import { Referencias } from '../../../model/referencias';
import { MomentDateAdapter, MAT_MOMENT_DATE_ADAPTER_OPTIONS } from '@angular/material-moment-adapter';
import { DateAdapter, MAT_DATE_FORMATS, MAT_DATE_LOCALE } from '@angular/material/core';
import { MatDatepicker } from '@angular/material/datepicker';
import * as $ from 'jquery';

import * as _moment from 'moment';
// tslint:disable-next-line:no-duplicate-imports
import { default as _rollupMoment, Moment } from 'moment';

const moment = _rollupMoment || _moment;

// See the Moment.js docs for the meaning of these formats:
// https://momentjs.com/docs/#/displaying/format/
export const MY_FORMATS = {
  parse: {
    dateInput: 'MM/YYYY',
  },
  display: {
    dateInput: 'MM/YYYY',
    monthYearLabel: 'MMM YYYY',
    dateA11yLabel: 'LL',
    monthYearA11yLabel: 'MMMM YYYY',
  },
};

@Component({
  templateUrl: './estimado.component.html',
  providers: [
    // `MomentDateAdapter` can be automatically provided by importing `MomentDateModule` in your
    // application's root module. We provide it at the component level here, due to limitations of
    // our example generation script.
    {
      provide: DateAdapter,
      useClass: MomentDateAdapter,
      deps: [MAT_DATE_LOCALE, MAT_MOMENT_DATE_ADAPTER_OPTIONS]
    },

    { provide: MAT_DATE_FORMATS, useValue: MY_FORMATS },
  ],
})
export class EstimadoComponent implements OnInit {

  formGlobal: FormGroup;
  formClien: FormGroup;
  formRefe: FormGroup;
  formClienRefe: FormGroup;
  myForm: FormGroup;
  proveedor;
  marca: Marca[];
  valorDis: number;
  valorRep: number;
  total: number;
  pointIndex = 0;
  controlCliente = new FormControl();
  controlRefe = new FormControl();
  filteredCliente: Observable<Cliente[]>;
  filteredRefe: Observable<Referencias[]>;
  global: boolean;
  clien: boolean;
  refe: boolean;
  clienrefe: boolean;
  public loading: boolean;
  t;

  constructor(private estimadoService: EstimadoService, private fb: FormBuilder) {
  }

  date = new FormControl(moment());

  chosenYearHandler(normalizedYear: Moment) {
    const ctrlValue = this.date.value;
    ctrlValue.year(normalizedYear.year());
    this.date.setValue(ctrlValue);
  }

  chosenMonthHandler(normalizedMonth: Moment, datepicker: MatDatepicker<Moment>) {
    const ctrlValue = this.date.value;
    ctrlValue.month(normalizedMonth.month());
    this.date.setValue(ctrlValue);
    datepicker.close();
  }

  ngOnInit() {

    this.formGlobal = this.fb.group({
      items: this.fb.array([this.fb.group({
        rep: ['0'],
        dis: ['0'],
        prov: [''],
      })])
    });

    this.formClien = this.fb.group({
      items: this.fb.array([this.fb.group({
        rep: ['0'],
        dis: ['0'],
        prov: [''],
      })])
    });

    this.formRefe = this.fb.group({
      rep: ['0'],
      dis: ['0'],
    });

    this.formClienRefe = this.fb.group({
      rep: ['0'],
      dis: ['0'],
    });

    this.estimadoService.cargarMar('').subscribe(mar => {
      this.marca = mar;
    });

  }

  precio() {
    $('.precio').on({
      'focus': function (event) {
        $(event.target).select();
      },
      'keyup': function (event) {
        $(event.target).val(function (index, value) {
          return value.replace(/\D/g, '')
            //.replace(/([0-9])([0-9]{2})$/, '$1.$2')
            .replace(/\B(?=(\d{3})+(?!\d)\.?)/g, ',');
        });
      }
    });
  }

  precioprueba(x: any) {
    console.log(x.dis);
    let y = x.dis.split(',');
    console.log(y);
    let t = '';
    for (let i = 0; i < y.length; i++) {
      t = t + y[i];
    }
    console.log(t);

  }

  get getItems() {
    return this.formGlobal.get('items') as FormArray;
  }

  get getItems2() {
    return this.formClien.get('items') as FormArray;
  }

  addItem() {
    const control = <FormArray>this.formGlobal.controls['items'];
    const control2 = <FormArray>this.formClien.controls['items'];
    control.push(this.fb.group({ rep: ['0'], dis: ['0'], prov: [''] }));
    control2.push(this.fb.group({ rep: ['0'], dis: ['0'], prov: [''] }));
    this.pointIndex++;
  }

  removeItem(index: number) {
    const control = <FormArray>this.formGlobal.controls['items'];
    const control2 = <FormArray>this.formClien.controls['items'];
    control.removeAt(index);
    control2.removeAt(index);
    this.pointIndex--;
  }

  submit(formValue: any) {

    this.loading = true;

    /*console.log(formValue);
    console.log(this.controlCliente.value);
    console.log(this.controlRefe.value);*/

    if (this.global === true) {
      // tslint:disable-next-line: max-line-length
      this.estimadoService.cargarEstimado(formValue.items, this.date.value, this.controlCliente.value, this.controlRefe.value).subscribe(r => {
        if (r[0] === 1) {
          this.t = true;
          this.loading = false;
        }
      });
    }

    if (this.clien === true) {
      // tslint:disable-next-line: max-line-length
      this.estimadoService.cargarEstimado(formValue.items, this.date.value, this.controlCliente.value, this.controlRefe.value).subscribe(r => {
        if (r[0] === 1) {
          this.t = true;
          this.loading = false;
        }
      });
    }

    if (this.refe === true) {
      formValue['fecha'] = this.date;
      this.estimadoService.cargarEstimado(null, formValue, this.controlCliente.value, this.controlRefe.value).subscribe(r => {
        if (r[0] === 1) {
          this.t = true;
          this.loading = false;
        }
      });
    }

    if (this.clienrefe === true) {
      formValue['fecha'] = this.date;
      this.estimadoService.cargarEstimado(null, formValue, this.controlCliente.value, this.controlRefe.value).subscribe(r => {
        if (r[0] === 1) {
          this.t = true;
          this.loading = false;
        }
      });
    }

  }

  mostrar(valorG: boolean, valorCli: boolean, valorRefe: boolean, valorCliRef: boolean) {
    this.global = valorG;
    this.clien = valorCli;
    this.refe = valorRefe;
    this.clienrefe = valorCliRef;

    if (this.clien === true) {
      this.controlRefe.reset();
      this.filteredCliente = this.controlCliente.valueChanges
        .pipe(
          map(value => typeof value === 'string' ? value : value),
          flatMap(value => value ? this.filterCliente(value) : [])
        );
    }

    if (this.refe === true) {
      this.controlCliente.reset();
      this.filteredRefe = this.controlRefe.valueChanges
        .pipe(
          map(value => typeof value === 'string' ? value : value),
          flatMap(value => value ? this.filterRefe(value) : [])
        );

    }

    if (this.clienrefe === true) {

      this.filteredCliente = this.controlCliente.valueChanges
        .pipe(
          map(value => typeof value === 'string' ? value : value),
          flatMap(value => value ? this.filterCliente(value) : [])
        );

      this.filteredRefe = this.controlRefe.valueChanges
        .pipe(
          map(value => typeof value === 'string' ? value : value),
          flatMap(value => value ? this.filterRefe(value) : [])
        );
    }

  }

  filterCliente(value: string): Observable<Cliente[]> {
    const filterValue = value.toLowerCase();
    return this.estimadoService.cargaCli(filterValue);
  }

  filterRefe(value2: string): Observable<Referencias[]> {
    const filterValue2 = value2.toLowerCase();
    return this.estimadoService.cargaRef(filterValue2);
  }

  getMarca(value, id) {
    if (value === '01' || value === '02' || value === '09' || value === '10') {
      document.getElementById('rep' + id).setAttribute('disabled', 'disabled');
      document.getElementById('dis' + id).removeAttribute('disabled');
    } else if (value === '16' || value === '21' || value === '22') {
      document.getElementById('dis' + id).setAttribute('disabled', 'disabled');
      document.getElementById('rep' + id).removeAttribute('disabled');
    } else {
      document.getElementById('dis' + id).removeAttribute('disabled');
      document.getElementById('rep' + id).removeAttribute('disabled');
    }
  }

  getMarcaClien(value, id) {
    if (value === '01' || value === '02' || value === '09' || value === '10') {
      document.getElementById('rep2' + id).setAttribute('disabled', 'disabled');
      document.getElementById('dis2' + id).removeAttribute('disabled');
    } else if (value === '16' || value === '21' || value === '22') {
      document.getElementById('dis2' + id).setAttribute('disabled', 'disabled');
      document.getElementById('rep2' + id).removeAttribute('disabled');
    } else {
      document.getElementById('dis2' + id).removeAttribute('disabled');
      document.getElementById('rep2' + id).removeAttribute('disabled');
    }
  }

  getMarcaRef(value) {
    if (value.id === '01' || value.id === '02' || value.id === '09' || value.id === '10') {
      document.getElementById('rep3').setAttribute('disabled', 'disabled');
      document.getElementById('dis3').removeAttribute('disabled');
    } else if (value.id === '16' || value.id === '21' || value.id === '22') {
      document.getElementById('dis3').setAttribute('disabled', 'disabled');
      document.getElementById('rep3').removeAttribute('disabled');
    } else {
      document.getElementById('dis3').removeAttribute('disabled');
      document.getElementById('rep3').removeAttribute('disabled');
    }
  }

  getMarcaCliRef(value) {
    if (value.id === '01' || value.id === '02' || value.id === '09' || value.id === '10') {
      document.getElementById('rep4').setAttribute('disabled', 'disabled');
      document.getElementById('dis4').removeAttribute('disabled');
    } else if (value.id === '16' || value.id === '21' || value.id === '22') {
      document.getElementById('dis4').setAttribute('disabled', 'disabled');
      document.getElementById('rep4').removeAttribute('disabled');
    } else {
      document.getElementById('dis4').removeAttribute('disabled');
      document.getElementById('rep4').removeAttribute('disabled');
    }
  }

}
