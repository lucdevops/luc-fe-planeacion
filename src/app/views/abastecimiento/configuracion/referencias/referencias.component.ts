import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { ReferenciasTipo } from '../../../../model/referenciasTipo';
import { ReferenciasService } from './referencias.service';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-referencias',
  templateUrl: './referencias.component.html',
})
export class ReferenciasComponent implements OnInit {

  refTipo: ReferenciasTipo[] = [];
  getTipo: ReferenciasTipo = new ReferenciasTipo();
  formRefTipo: FormGroup;

  constructor(private refService: ReferenciasService, private fb: FormBuilder) { }

  ngOnInit() {
    this.refService.cargarRefTipo().subscribe(r => {
      this.refTipo = r;
    });

    this.formRefTipo = this.fb.group({
      tipo: ['', Validators.required],
      dias: ['', Validators.required]
    });

  }

  guardarRefTipo(form: any) {
    this.refService.guardarRefTipo(form).subscribe(r => {
      document.getElementById('closeC').click();
      //location.reload();
      /*console.log(r);
      if (r === true) {
        window.location.href = 'abastecimiento/configuracion';
      }*/
    });
  }

  getRefTipo(valor: ReferenciasTipo): void {
    this.getTipo = valor;
  }

  modificarRefTipo() {
    this.refService.modificarRefTipo(this.getTipo).subscribe(r => {
      //console.log(r);
      document.getElementById('closeM').click();
      //window.location.href = '#/abastecimiento/configuracion';
    });
  }

}
