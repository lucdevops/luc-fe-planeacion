import { ReferenciasTipo } from '../../../../model/referenciasTipo';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Rutas } from '../../../../rutas';

@Injectable({
  providedIn: 'root'
})
export class ReferenciasService {

  constructor(private http: HttpClient, private r: Rutas) { }

  cargarRefTipo(): Observable<ReferenciasTipo[]> {
    return this.http.get<ReferenciasTipo[]>(this.r['datosPlaneacion'] + '/cargarRefTipo');
  }

  guardarRefTipo(form: any) {
    return this.http.get(this.r['crearPlaneacion'] + '/crearRefTipo/' + form.tipo + '/' + form.dias);
  }

  modificarRefTipo(modf: ReferenciasTipo) {
    return this.http.get(this.r['crearPlaneacion'] + '/modificarRefTipo/' + modf.tipo + '/' + modf.dias);
  }

}
