import { ProveedorService } from './proveedor.service';
import { EstimadoService } from './../../estimado/estimado.service';
import { Component, OnInit, Input } from '@angular/core';
import { FormGroup, FormControl, FormBuilder, Validators } from '@angular/forms';
import { Marca } from '../../../../model/marca';

@Component({
  selector: 'app-proveedor',
  templateUrl: './proveedor.component.html'
})
export class ProveedorComponent implements OnInit {

  getMarca: Marca = new Marca();
  marca: Marca[] = [];
  intForm: FormGroup;

  constructor(private estimadoService: EstimadoService, private provService: ProveedorService) {
  }

  ngOnInit() {
    this.estimadoService.cargarMar('').subscribe(v => {
      this.marca = v;
    });
  }

  getMarcaId(marca: Marca): void {
    this.estimadoService.cargarMar(marca).subscribe(v => {
      this.getMarca = v[0];
    });
  }

  guardarForm() {
    this.provService.modificarProveedor(this.getMarca).subscribe(r => {
      console.log(r);
      if (r === true) {
        document.getElementById('close').click();
      }
    });
  }

  habilitar(id: string) {

    const validation = document.getElementById(id).getAttribute('disabled');
    console.log(validation)
    document.getElementById(id).removeAttribute('disabled');
    /*if (validation === 'disabled') {
      document.getElementById(id).removeAttribute('disabled');
      document.getElementById(id).focus();
    }*/

  }

}
