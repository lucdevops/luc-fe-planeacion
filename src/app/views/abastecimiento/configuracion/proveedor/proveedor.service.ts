import { Rutas } from './../../../../rutas';
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Marca } from '../../../../model/marca';

@Injectable({
  providedIn: 'root'
})
export class ProveedorService {

  constructor(private http: HttpClient, private r: Rutas) { }

  modificarProveedor(marca: Marca) {
    return this.http.get(this.r['crearPlaneacion'] + '/modificarProveedor/' + marca.timeRepo + '/' + marca.leadTime + '/' + marca.id);
  }
}
