import { ProgressbarModule } from 'ngx-bootstrap/progressbar';
import { ReferenciasMaestroComponent } from './referenciasMaestro/referenciasMaestro.component';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { BsDropdownModule } from 'ngx-bootstrap/dropdown';
import { MaestrosRoutingModule } from './maestros-routing.module';
import { ModalModule } from 'ngx-bootstrap/modal';
import { DemoMaterialModule } from '../../material-module';
import { CommonModule, DatePipe } from '@angular/common';
import { HttpClientModule } from '@angular/common/http';
import { RouterModule } from '@angular/router';
import { ProveedoresMaestroComponent } from './proveedoresMaestro/proveedoresMaestro.component';
import { TabsModule } from 'ngx-bootstrap/tabs';
import { CollapseModule } from 'ngx-bootstrap/collapse';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';

@NgModule({
  imports: [
    MaestrosRoutingModule,
    CommonModule,
    FormsModule,
    BsDropdownModule.forRoot(),
    ModalModule.forRoot(),
    CollapseModule.forRoot(),
    ProgressbarModule.forRoot(),
    DemoMaterialModule,
    ReactiveFormsModule,
    NgbModule,
    HttpClientModule,
    TabsModule,
    RouterModule
  ],
  declarations: [
    ProveedoresMaestroComponent,
     ReferenciasMaestroComponent
  ],
  providers: [DatePipe]
})
export class MaestrosModule { }
