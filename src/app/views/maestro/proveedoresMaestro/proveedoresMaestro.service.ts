import { Rutas } from '../../../rutas';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { DatePipe } from '@angular/common';
import { Cliente } from '../../../model/clientes';
import { forkJoin } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ProveedoresMaestroService {

  constructor(private http: HttpClient, private r: Rutas, private dp: DatePipe) { }

  cargarProveedores() {
    return this.http.get<Cliente[]>(this.r['datosWMS'] + '/cargarProveedores');
  }

  cargarDatosProveedores(nit) {
    return this.http.get<any[]>(this.r['datosWMS'] + '/cargarDatosProveedores/' + nit);
  }

  guardarDatosProveedorWMS(fDatos: boolean, fAbastecimiento: boolean, form: any, nit: string) {

    console.log(form);
    

    /*if (fDatos === true) {
      let rutas = [];
      let rute;
      rute = this.http.get<any>(this.r['crearCP'] + '/modificarDatosGeneralesProveedoresCP/' + form.nombreRazonSocial + '/' + form.correo + '/' + nit)
      rutas.push(rute);
      rute = this.http.get<any>(this.r['crearWMS'] + '/modificarDatosGeneralesProveedores/' + form.nombreRazonSocial + '/' + form.correo + '/' + nit)
      rutas.push(rute);

      return forkJoin(rutas);
    }

    if (fAbastecimiento === true) {
      return this.http.get<any>(this.r['crearWMS'] + '/modificarDatosAbatecimientoProveedores/' + form.cantidadCajas + '/' + form.valor + '/' + nit)
    }*/
  }
}
