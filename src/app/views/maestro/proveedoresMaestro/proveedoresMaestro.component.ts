import { Component, OnInit } from "@angular/core";
import * as _moment from 'moment';
import { ProveedoresMaestroService } from './proveedoresMaestro.service';
import { Cliente } from '../../../model/clientes';
import { FormGroup, FormBuilder } from '@angular/forms';

@Component({
  selector: 'app-proveedores-maestro',
  templateUrl: './proveedoresMaestro.component.html',
})
export class ProveedoresMaestroComponent implements OnInit {

  proveedores: Cliente[];
  formDatos: FormGroup;
  formAbastecimiento: FormGroup;
  abastecimiento: boolean;
  datos: boolean;
  nit;

  constructor(private provMaestroService: ProveedoresMaestroService, private fb: FormBuilder) { }

  ngOnInit() {
    this.provMaestroService.cargarProveedores().subscribe(r => {
      this.proveedores = r;
    });

    this.formDatos = this.fb.group({
      nombreRazonSocial: [],
      correo: [],
    });

    this.formAbastecimiento = this.fb.group({
      cantidadCajas: [],
      valor: [],
    });
  }

  cargarDatosProveedor(nit: string) {
    this.nit = nit;
    this.provMaestroService.cargarDatosProveedores(this.nit).subscribe(r => {

      this.formDatos.patchValue({
        nombreRazonSocial: r[0],
        correo: r[1]
      })

      this.formAbastecimiento.patchValue({
        cantidadCajas: r[2],
        valor: r[3],
      })
    })
  }

  mostrar(valorDatos: boolean, valorAbastecimiento: boolean) {
    this.abastecimiento = valorAbastecimiento;
    this.datos = valorDatos;
  }


  submit(formValue: any) {
    this.provMaestroService.guardarDatosProveedorWMS(this.datos, this.abastecimiento, formValue, this.nit)
  }


}
