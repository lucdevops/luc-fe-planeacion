import { Component, OnInit } from '@angular/core';
import { ReferenciasMaestroService } from './referenciasMaestro.service';
import { Referencias } from '../../../model/referencias';
import { FormGroup, FormBuilder } from '@angular/forms';

@Component({
  selector: 'app-referencias-maestro',
  templateUrl: './referenciasMaestro.component.html',
})

export class ReferenciasMaestroComponent implements OnInit {

  constructor(private refeMstrService: ReferenciasMaestroService, private fb: FormBuilder) {
  }

  formDescripcion: FormGroup;
  formStock: FormGroup;
  referencias: Referencias[];
  refe: Referencias;
  descripcion: boolean;
  stock: boolean;
  codigoBarras: string;

  ngOnInit() {
    this.refeMstrService.cargarReferencias().subscribe(r => {
      this.referencias = r;
    });

    this.formDescripcion = this.fb.group({
      nombre: [],
      codigo: [],
      refFab: []
    });

    this.formStock = this.fb.group({
      novedad: [],
      caja: [],
      display: [],
      tipoRefe: [],
    });
  }

  cargarDatosReferencia(codigo: string) {
    this.codigoBarras = codigo;
    this.refeMstrService.cargarDatosReferencia(codigo).subscribe(r => {
      console.log(r);
    })
  }

  mostrar(valorDescripcion: boolean, valorStock: boolean) {
    this.descripcion = valorDescripcion;
    this.stock = valorStock;
  }


  submit(formValue: any) {

    if (this.descripcion === true) {
      // tslint:disable-next-line: max-line-length
      this.refeMstrService.guardarDatosReferenciaWMS(this.descripcion, this.stock, formValue, this.codigoBarras).subscribe(r => {
        console.log(r);
      });

      this.refeMstrService.guardarDatosReferenciaCP(this.descripcion, this.stock, formValue, this.codigoBarras).subscribe(r => {
        console.log(r);
      });
    }

    if (this.stock === true) {
      // tslint:disable-next-line: max-line-length
      this.refeMstrService.guardarDatosReferenciaWMS(this.descripcion, this.stock, formValue, this.codigoBarras).subscribe(r => {
        console.log(r);
      });
    }
  }



}
