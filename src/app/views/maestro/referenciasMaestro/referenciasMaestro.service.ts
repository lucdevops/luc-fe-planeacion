import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Rutas } from '../../../rutas';
import { Referencias } from '../../../model/referencias';
import { forkJoin } from 'rxjs';
import { ReferenciasTipo } from '../../../model/referenciasTipo';

@Injectable({
  providedIn: 'root'
})
export class ReferenciasMaestroService {

  constructor(private http: HttpClient, private r: Rutas) { }

  cargarReferencias() {
    return this.http.get<Referencias[]>(this.r['datosWMS'] + '/cargaAllRefe')
  }

  cargarDatosReferencia(codigo: string) {
    return this.http.get<any[]>(this.r['datosWMS'] + '/cargaDatosRefe/' + codigo)
  }

  guardarDatosReferenciaWMS(fDescrip: boolean, fStock: boolean, form: any, codigo: string) {
    if (fDescrip === true) {
      this.http.get<any>(this.r['crearCP'] + '/modificarDatosGeneralesReferenciaCP/' + form.nombre + '/' + form.codigo + '/' + form.refFab + '/' + codigo)
      return this.http.get<any>(this.r['crearWMS'] + '/modificarDatosGeneralesReferencia/' + form.nombre + '/' + form.codigo + '/' + form.refFab + '/' + codigo)
    }

    if (fStock === true) {
      return this.http.get<any>(this.r['crearWMS'] + '/modificarDatosStockReferencia/' + form.novedad + '/' + form.caja + '/' + form.display + '/' + form.tipoRefe + '/' + codigo)
    }

  }

  guardarDatosReferenciaCP(fDescrip: boolean, fStock: boolean, form: any, codigo: string) {
    if (fDescrip === true) {
      return this.http.get<any>(this.r['crearCP'] + '/modificarDatosGeneralesReferenciaCP/' + form.nombre + '/' + form.codigo + '/' + form.refFab + '/' + codigo)
    }

    if (fStock === true) {
      return this.http.get<any>(this.r['crearCP'] + '/modificarDatosStockReferenciaCP/' + form.novedad + '/' + form.caja + '/' + form.display + '/' + form.tipoRefe + '/' + codigo)
    }

  }

}
