import { ReferenciasMaestroComponent } from './referenciasMaestro/referenciasMaestro.component';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ProveedoresMaestroComponent } from './proveedoresMaestro/proveedoresMaestro.component';

const routes: Routes = [
  {
    path: '',
    data: {
      title: 'Maestros'
    },
    children: [
      /*{
        path: '',
        redirectTo: 'agendados'
      },*/
      {
        path: 'referenciasmaestro',
        component: ReferenciasMaestroComponent,
        data: {
          title: 'Maestro Referencias'
        }
      },
      {
        path: 'proveedoresmaestro',
        component: ProveedoresMaestroComponent,
        data: {
          title: 'Maestro Proveedores'
        }
      },
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class MaestrosRoutingModule {}
