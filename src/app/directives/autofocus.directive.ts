import { Directive } from '@angular/core';
import { AfterContentInit } from '@angular/core';
import { ElementRef } from '@angular/core';
import { OnChanges } from '@angular/core';
import { OnDestroy } from '@angular/core';
import { SimpleChanges } from '@angular/core';

@Directive({
  selector: '[appAutofocus]',
  // tslint:disable-next-line: no-inputs-metadata-property
  inputs: [
    'shouldFocusElement: appAutofocus',
    'timerDelay: autofocusDelay'
  ]
})
export class AutofocusDirective implements AfterContentInit, OnChanges, OnDestroy {

  public shouldFocusElement: any;
  public timerDelay: number | string;

  private elementRef: ElementRef;
  private timer: number;

  constructor(elementRef: ElementRef) {

    this.elementRef = elementRef;
    this.shouldFocusElement = '';
    this.timer = null;

  }

  public ngAfterContentInit(): void {

    if (this.shouldFocusElement === '') {
      this.startFocusWorkflow();
    }

  }

  public ngOnChanges(changes: SimpleChanges): void {

    if (changes.shouldFocusElement) {

      (this.shouldFocusElement)
        ? this.startFocusWorkflow()
        : this.stopFocusWorkflow()
        ;

    }

  }

  public ngOnDestroy(): void {
    this.stopFocusWorkflow();
  }

  private startFocusWorkflow(): void {

    this.timer = setTimeout(
      (): void => {
        this.timer = null;
        this.elementRef.nativeElement.focus();
      }
    );

  }

  private stopFocusWorkflow(): void {

    clearTimeout(this.timer);
    this.timer = null;

  }

}
