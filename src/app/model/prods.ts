export class Prods {
    producto: string;
    cantidad: number;
    items: Prods[];
    precio: number;
    descuento: number;
    impuesto: number;
    total: number;
}
