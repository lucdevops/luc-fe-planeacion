export class Cliente {
  nombres: string;
  nit: string;
  // tslint:disable-next-line: max-line-length
  autoretenedor: number; // numero del pedidos, lo envio por este campo para poder manejarlo en las rutas y cambiar el estado en la bd con este numero
  ean: string;
  codigoLuc: string;
  listaPrecio: string;
  ciudad: string;
  direccion: string;
  nitReal: number;
  vendedor: number;
  vd: string;
  pais: string;
  contribuyente: number;
  concep1: string;
  concep6: string;
  ypais: string;
  ydpto: number;
  yciudad: number;
  diaRecibo: string;
  id: string;
  fechaInicio: string;
  fechaFin: string;
}
