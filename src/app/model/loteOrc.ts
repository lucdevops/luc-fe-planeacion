export class LoteOrc {
  tipo: string;
  numero: string;
  codigo: string;
  cantidad: number;
  lote: string;
  vencimiento: string;
}
