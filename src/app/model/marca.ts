export class Marca {
  nombre: string;
  nit: number;
  id: number;
  timeRepo: number;
  leadTime: number;
}
