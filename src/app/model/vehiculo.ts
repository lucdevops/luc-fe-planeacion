export class Vehiculo {
    placa: string;
    fechasoat: string;
    fechatec: string;
    peso: number;
    cubicaje: number;
    clasevehiculo: string;
    tipovehiculo: string;

}