export class PreOrden {
  tipo: string;
  anio: number;
  mes: number;
  codigo: string;
  descripcion: string;
  marca: string;
  timeRepo: number;
  leadTime: number;
  tipoRef: string;
  invSegurity: number;
  cntEstimado: number;
  stock: number;
}
