import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { DefaultLayoutComponent } from './containers';
import { P404Component } from './views/error/404.component';
import { P500Component } from './views/error/500.component';
import { LoginComponent } from './views/login/login.component';
import { RegisterComponent } from './views/register/register.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { DemoMaterialModule } from './material-module';
import { CommonModule } from '@angular/common';

export const routes: Routes = [
  {
    path: '',
    redirectTo: 'dashboard',
    pathMatch: 'full',
  },
  {
    path: '404',
    component: P404Component,
    data: {
      title: 'Page 404'
    }
  },
  {
    path: '500',
    component: P500Component,
    data: {
      title: 'Page 500'
    }
  },
  {
    path: 'login',
    component: LoginComponent,
    data: {
      title: 'Login Page'
    }
  },
  {
    path: 'register',
    component: RegisterComponent,
    data: {
      title: 'Register Page'
    }
  },
  {
    path: '',
    component: DefaultLayoutComponent,
    data: {
      title: 'Home'
    },
    children: [
      {
        path: 'dashboard',
        loadChildren: () => import('./views/dashboard/dashboard.module').then(m => m.DashboardModule)
      },
      {
        path: 'planilla',
        loadChildren: () => import('./views/planilla/planilla.module').then(m => m.PlanillaModule)
      },
      {
        path: 'bodega',
        loadChildren: () => import('./views/bodega/bodega.module').then(m => m.BodegaModule)
      },
      {
        path: 'abastecimiento',
        loadChildren: () => import('./views/abastecimiento/abastecimiento.module').then(m => m.AbastecimientoModule)
      },
      {
        path: 'vidas',
        loadChildren: () => import('./views/vidas/vidas.module').then(m => m.VidasModule)
      },
      {
        path: 'maestro',
        loadChildren: () => import('./views/maestro/maestros.module').then(m => m.MaestrosModule)
      }
    ]
  },
  { path: '**', component: P404Component }
];

@NgModule({
  imports: [RouterModule.forRoot(routes), CommonModule, DemoMaterialModule, NgbModule, FormsModule, ReactiveFormsModule],
  exports: [RouterModule]
})
export class AppRoutingModule { }
